import { Todo } from '../../todo';

export const TODO_ADD 						= '[TODO] Add';
export const TODO_UPDATE 					= '[TODO] Update';
export const TODO_TOGGLE 					= '[TODO] Toggle';
export const TODO_REMOVE 					= '[TODO] Remove';
export const TODO_REMOVE_ALL 				= '[TODO] Remove All';
export const TODO_DISPLAY_ACTIVE_TODOS 		= '[TODO] Display Active Todos';
export const TODO_DISPLAY_COMPLETED_TODOS 	= '[TODO] Display COMPLETED Todos';
export const TODO_DISPLAY_ALL_TODOS 		= '[TODO] Display ALL Todos';

export interface TodoState {
	todos: Todo[];
}

export const initialState: TodoState = {
	todos: [],
};

let todoId = 0;

export function todoReducer(state = initialState, action: any) {

	switch (action.type) {

		case TODO_ADD:
			return {
				todos: [
					...state.todos,
					{
						id: todoId++,
						title: action.payload.todo,
						status: false
					}
				]
			}; 

		default:
			return state;
	}
}
