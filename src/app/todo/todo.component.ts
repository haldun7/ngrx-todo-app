import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as todoStore from './store/';

import { Todo } from './todo';

@Component({
	selector: 'todo-component',
	templateUrl: './todo-component.html',
	styleUrls: ['./todo.scss']
})

export class TodoComponent implements OnInit {
 
	public todos$		: Observable<Todo[]>;
	public newTodo		: string;
	public editTodoTitle: string;
	public completed	: boolean;
 
	constructor(private store: Store<todoStore.TodoState>) { }

	ngOnInit() {
		this.todos$ = this.store.select('todos');
	}

	addTodo() {
		this.store.dispatch({
			type: todoStore.TODO_ADD,
			payload: {todo: this.newTodo}
		});
	}

	editTodo(id, title) { }

	checkTodo(todo) { }

	removeTodo(todoId) { }

	clearTodos() { }
	
	watchTodos(index, item) {
		return item.id;
	}
}
